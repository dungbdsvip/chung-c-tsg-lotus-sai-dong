# Chung cư TSG Lotus Sài Đồng
Dự án [TSG Lotus Sài Đồng](http://northernhomes.vn/shophouse-va-chung-cu-cao-cap-tsg-lotus-sai-dong-long-bien.html) phát triển với các loại hình Shophouse thương mại, chung cư cao cấp, trung tâm thương mại, trường mần non phát triển đồng bộ hiện đại được xây dựng tại  số 190 Sài Đồng, Long Biên, Hà Nội.  TSG Lotus Sài Đồng khẳng định vị thế ưu việt vượt trội với hệ thống tiện ích khu vực đẳng cấp đầy đủ, hiện đại đảm bảo chất lượng sống tối ưu cho cư dân.
🔸 Tên dự án: Khu nhà ở cao tầng kết hợp dịch vụ thương mại nhà ở thấp tầng và nhà trẻ mẫu giáo
🔸 Tên thương mại: TSG Lotus Sài Đồng
🔸 Địa chỉ: Số 190 Phố Sài Đồng, phường Việt Hưng, quận Long Biên, Tp.Hà Nội
🔸 Chủ đầu tư: Công ty Cổ phần Bất động sản Sài Đồng
🔸 Đơn vị thi công:  Công ty cổ phần đầu tư và xây dựng TSG (TSG I&C)
🔸 Đơn vị tư vấn thiết kế: Công ty Cổ phần Xây dựng RECO
🔸 Tổng diện tích: 10.015 m2
🔸 Diện tích xây dựng: 3.898 m2
🔸 Mật độ xây dựng: 40%
🔸 Dự kiến bàn giao:  Tháng 3/2020
🔸 Diện tích đất chung cư: 3.452 m2
🔸 Diện tích đất xây dựng: 2.021 m2
🔸 Tổng diện tích sàn: 46.408 m2
🔸Shophouse thương mại: 21 căn Shophouse
Bản quyền bài viết thuộc về : [http://northernhomes.vn/shophouse-va-chung-cu-cao-cap-tsg-lotus-sai-dong-long-bien.html](http://northernhomes.vn/shophouse-va-chung-cu-cao-cap-tsg-lotus-sai-dong-long-bien.html)
